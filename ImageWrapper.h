// ---------------------------------------------------------------------------
// ImageWrapper.h
// Wrapper around the default Image class. Provides additional functionality,
// such as:
//
//		Saving an image
//		Hough Transform
//		Sobel Filter
//
// Author: Andrew Miloslavsky
// Date: October 21st, 2015
// ---------------------------------------------------------------------------

#ifndef _IMAGE_WRAPPER_
#define _IMAGE_WRAPPER_
#include "Image.h"
#include <vector>

class ImageWrapper : public Image {

public:
	// ---------------------------------------------------------------------------
	// Ctors
	// Purpose: Runs the sobel filter on this image. 
	//			
	//
	// Parameters:
	//		Parameter 1: Image OR Path to input file
	// ---------------------------------------------------------------------------
	ImageWrapper( const char* );
	ImageWrapper( const Image& );
	
	// ---------------------------------------------------------------------------
	// Dtor
	// ---------------------------------------------------------------------------
	~ImageWrapper();
	
	// ---------------------------------------------------------------------------
	// SAVE
	// Purpose: Saves the image.
	//			
	//
	// Parameters:
	//		Parameter 1: path for output file
	// ---------------------------------------------------------------------------
	int save( const char* ) const;
	
	// ---------------------------------------------------------------------------
	// GET_CENTER
	// Purpose: Gets the center of 1 object in an image.
	//
	// Parameters:
	//		Parameter 1: Threshold
	// ---------------------------------------------------------------------------
	std::pair< double, double > get_center( const int threshold ) const;
	
	// ---------------------------------------------------------------------------
	// GET_RADIUS
	// Purpose: Gets the radius of 1 object in an image.
	//
	// Parameters:
	//		Parameter 1: Threshold
	// ---------------------------------------------------------------------------
	double get_radius( const int ) const;
	
	// ---------------------------------------------------------------------------
	// GET_BOUNDARIES
	// Purpose: Gets the boundaries of 1 object in an image.
	//
	// Parameters:
	//		Parameter 1: Threshold
	// ---------------------------------------------------------------------------
	std::vector< int > get_boundaries( const int ) const;
	
	// ---------------------------------------------------------------------------
	// GET_BRIGHTEST_PIXEL
	// Purpose: Gets the brightest pixel in the image.
	//
	// Parameters:
	//		NONE
	// ---------------------------------------------------------------------------
	std::pair< int, std::pair< int, int > > get_brightest_pixel( void ) const;
	
	// ---------------------------------------------------------------------------
	// GET_Z
	// Purpose: Gets the Z coordinate at a point on the image.
	//
	// Parameters:
	//		Parameter 1: radius
	//		Parameter 2: center coordinates
	//		Parameter 3: bright point coordinates
	// ---------------------------------------------------------------------------
	double get_z( const double radius, const std::pair< int, int >, const std::pair< int, int > ) const;
	
	// ---------------------------------------------------------------------------
	// GET_MAGNITUDE
	// Purpose: Gets the magnitude.
	//
	// Parameters:
	//		Parameter 1: center coordinates
	//		Parameter 2: brightest point coordinates
	//		Parameter 3: z-cz
	// ---------------------------------------------------------------------------
	double get_magnitude( const std::pair< int, int >, std::pair< int, int >, const double z ) const;
	
	// ---------------------------------------------------------------------------
	// DRAW_NORMALS
	// Purpose: Draws the normals on the image.
	//
	// Parameters:
	//		Parameter 1: Vector containing 3 XYZ vectors ( 9 elements )
	//		Parameter 2: step
	//		Parameter 3: threshold
	//		Parameter 4: image2
	//		Parameter 5: image3
	// ---------------------------------------------------------------------------
	void draw_normals( std::vector< double >, const int, const int, const ImageWrapper&, const ImageWrapper& );
	
	// ---------------------------------------------------------------------------
	// DRAW_ALBEDO
	// Purpose: Draws the albedo of the image.
	//
	// Parameters:
	//		Parameter 1: Vector containing 3 XYZ vectors ( 9 elements )
	//		Parameter 2: threshold
	//		Parameter 3: image2
	//		Parameter 4: image3
	// ---------------------------------------------------------------------------
	void draw_albedo( std::vector< double >, const int, const ImageWrapper&, const ImageWrapper& );
	
	// ---------------------------------------------------------------------------
	// SOBEL_FILTER
	// Purpose: Runs the sobel filter on this image. 
	//			
	// Parameters:
	//		NONE
	// ---------------------------------------------------------------------------
	void sobel_filter( void );
	
	// ---------------------------------------------------------------------------
	// GREYSCALE_TO_BINARY
	// Purpose: Converts a greyscale image to a binary image using a threshold 
	//			value.
	//
	// Parameters:
	//		Parameter 1: Threshold value
	// ---------------------------------------------------------------------------
	void binary_filter( const double );
	
	// ---------------------------------------------------------------------------
	// HOUGH_TRANSFORM
	// Purpose: Runs the Hough Transform on this image, producing an output
	//			of its Hough Space.
	//			
	// Parameters:
	//		NONE
	// ---------------------------------------------------------------------------
	void hough_transform( void );
	
	
	// ---------------------------------------------------------------------------
	// HOUGH_LINES
	// Purpose: Draws lines on this image based on the Hough Space in parameter 2. 
	//			
	//
	// Parameters:
	//		Parameter 1: Threshold for filtering lines
	//		Parameter 2: Hough space
	// ---------------------------------------------------------------------------
	void hough_lines( const int, const Image& );
	
private:
	const float PI;
};

#endif