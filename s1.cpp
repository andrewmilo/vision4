// ---------------------------------------------------------------------------
// s1.cpp
// Computes the centroid and radius of an image object.
// 
// Author: Andrew Miloslavsky
// Date: November 15th, 2015
// ---------------------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <cstdlib>
#include "ImageWrapper.h"

int main(int argc, char** argv){
	
	if( argc < 4 ) return -1;
	
	const char* INPUT_IMAGE = argv[1];
	const int THRESHOLD = std::atoi( argv[2] );
	const char* OUTPUT_FILE = argv[3];
	
	// Load image
	ImageWrapper* image = new ImageWrapper( INPUT_IMAGE );
	
	// Run the binary filter on the image
	image->binary_filter( THRESHOLD );

	// Create output file
	std::ofstream out ( OUTPUT_FILE );
	
	// Get center of the object in the image
	const std::pair< double, double > CENTER 
			= image->get_center( THRESHOLD );
	
	// Write center to `OUTPUT_FILE`
	out << CENTER.first
		<< " "
		<< CENTER.second
		<< " "
		<< image->get_radius( THRESHOLD ); // write radius
		
	delete image; // deallocate image

	return 0;
}