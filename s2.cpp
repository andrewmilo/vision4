// ---------------------------------------------------------------------------
// s2.cpp
// Computes the normal vectors of the brightest spots on an image object.
// 
// Author: Andrew Miloslavsky
// Date: November 15th, 2015
// ---------------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include "ImageWrapper.h"

int main(int argc, char** argv){
	
	if( argc < 6 ) return -1;
	
	const char* INPUT_FILE = argv[1];
	const char* IMAGE1 = argv[2];
	const char* IMAGE2 = argv[3];
	const char* IMAGE3 = argv[4];
	const char* OUTPUT_FILE = argv[5];
	
	// Create text file
	std::ifstream is ( INPUT_FILE );
	
	// Temp x, y coordinates and r, radius
	std::string x_s;
	std::string y_s;
	std::string r_s;
	
	// Parse the line in text file
	std::string line;
	if( std::getline( is, line ) ){
		
		std::istringstream iss ( line );
		
		iss >> x_s; // read x
		iss >> y_s; // read y
		iss >> r_s; // read r
	}
	
	// Convert x,y,z to doubles
	const double x = atof( x_s.c_str() );
	const double y = atof( y_s.c_str() );
	const double r = atof( r_s.c_str() );
	
	// Load the 3 images
	ImageWrapper* im1 = new ImageWrapper( IMAGE1 );
	ImageWrapper* im2 = new ImageWrapper( IMAGE2 );
	ImageWrapper* im3 = new ImageWrapper( IMAGE3 );
	
	// Cache center
	const std::pair< int, int > center = std::make_pair( x, y );
	
	// Get brightest pixels
	const std::pair< int, std::pair< int, int > > b1 = im1->get_brightest_pixel();
	const std::pair< int, std::pair< int, int > > b2 = im2->get_brightest_pixel();
	const std::pair< int, std::pair< int, int > > b3 = im3->get_brightest_pixel();
	
	// Get Z coordinates for each bright spot
	const double z1 = im1->get_z( r, center, b1.second );
	const double z2 = im2->get_z( r, center, b2.second );
	const double z3 = im3->get_z( r, center, b3.second );
	
	// Calculate magnitudes
	const double mag1 = im1->get_magnitude( center, b1.second, z1 );
	const double mag2 = im2->get_magnitude( center, b2.second, z2 );
	const double mag3 = im3->get_magnitude( center, b3.second, z3 );
	
	std::ofstream os ( OUTPUT_FILE ); // Open output file
	
	os << ( ( b1.second.first - x ) / mag1 ) * b1.first // (x - cx) / magnitude * sphere0 brightest point
	   << " " 
	   << ( ( b1.second.second - y ) / mag1 ) * b1.first // (y - cy) / magnitude * sphere0 brightest point
	   << " " 
	   << ( z1 / mag1 ) * b1.first // (z - zc) / magnitude * sphere0 brightest point
	   << "\n"
	   << ( ( b2.second.first - x ) / mag2 ) * b2.first // (x-cx) / magnitude * sphere1 brightest point
	   << " " 
	   << ( ( b2.second.second - y ) / mag2 ) * b2.first // (y-cy) / magnitude * sphere1 brightest point
	   << " " 
	   << ( z2 / mag2 ) * b2.first // (z - zc) / magnitude * sphere1 brightest point
	   << "\n"
	   << ( ( b3.second.first - x ) / mag3 ) * b3.first // (x-cx) / magnitude * sphere2 brightest point
	   << " " 
	   << ( ( b3.second.second - y ) / mag3 ) * b3.first // (y-cy) / magnitude * sphere2 brightest point
	   << " " 
	   << ( z3 / mag3 ) * b3.first // (z - zc) / magnitude * sphere2 brightest point
	   << "\n";
	   
	// Deallocate images
	delete im1;
	delete im2;
	delete im3;
	
	return 0;
}