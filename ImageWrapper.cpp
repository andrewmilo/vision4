// ---------------------------------------------------------------------------
// ImageWrapper.cpp
// Wrapper around the default Image class. Provides additional functionality,
// such as:
//
//		Saving an image
//		Hough Transform
//		Binary Filter
//		Sobel Filter
//
// Author: Andrew Miloslavsky
// Date: October 21st, 2015
// ---------------------------------------------------------------------------

#include "ImageWrapper.h"
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <vector>
#include <limits.h>
#include <algorithm>

// ---------------------------------------------------------------------------
// Ctors
// Purpose: Initializes this image.
//			
//
// Parameters:
//		Parameter 1: Image OR Path to input file
// ---------------------------------------------------------------------------
ImageWrapper::ImageWrapper( const char* PATH ) : PI( 3.14159265358979323846 ) {
	
	// Try to read file into this object
	if( readImage( this, PATH ) ){
		
		if( !PATH )
			throw std::invalid_argument( "Invalid input file" );
		
		throw std::bad_alloc();
	}
}

ImageWrapper::ImageWrapper( const Image& image ) : Image( image ), PI( 3.14159265358979323846 ) { }

// ---------------------------------------------------------------------------
// Dtor
// ---------------------------------------------------------------------------
ImageWrapper::~ImageWrapper( void ){ }

// ---------------------------------------------------------------------------
// SAVE
// Purpose: Saves the image.
//			
//
// Parameters:
//		Parameter 1: path for output file
// ---------------------------------------------------------------------------
int ImageWrapper::save( const char* PATH ) const {
	
	return writeImage( this, PATH );
}

// ---------------------------------------------------------------------------
// GET_CENTER
// Purpose: Gets the center of 1 object in an image.
//
// Parameters:
//		NONE
// ---------------------------------------------------------------------------
std::pair< double, double > ImageWrapper::get_center( const int threshold ) const {

	double xsum = 0;
	double ysum = 0;
	double totalpixels = 0;
	
	// Compute centroid
	for( int x = 0; x < this->getNRows(); x++ ){
		
		for( int y = 0; y < this->getNCols(); y++ ){
			
			if( this->getPixel( x, y ) > threshold){
				
				++totalpixels;
				xsum += x; // add x
				ysum += y; // add y
			}
		}
	}
	
	// Return x,y
	return std::make_pair ( xsum / totalpixels 
						  , ysum / totalpixels );
}

// ---------------------------------------------------------------------------
// GET_RADIUS
// Purpose: Gets the radius of 1 object in an image.
//
// Parameters:
//		NONE
// ---------------------------------------------------------------------------
double ImageWrapper::get_radius( const int threshold ) const {
	
	// Get boundary images
	std::vector< int > bounds = this->get_boundaries( threshold );
	
	return ( double( ( bounds[1] - bounds[0] ) + ( bounds[3] - bounds[2] ) ) / 2.0 ) / 2.0;
}

// ---------------------------------------------------------------------------
// GET_BOUNDARIES
// Purpose: Gets the boundaries of 1 object in an image.
//
// Parameters:
//		NONE
// ---------------------------------------------------------------------------
std::vector< int > ImageWrapper::get_boundaries( const int threshold ) const {
	
	// Initialize values
	int xmin = INT_MAX
		, ymin = INT_MAX
		, xmax = -1
		, ymax = -1;

	// Traverse image to find object boundaries
	for(int x = 0; x < this->getNRows(); x++){
		for(int y = 0; y < this->getNCols(); y++){
			
			const int px = this->getPixel( x, y );
			
			// Get item boundaries
			if( px > threshold ){
			
				if( x < xmin ) xmin = x; // x point minimum
				if( x > xmax ) xmax = x; // x point maximum
				if( y < ymin ) ymin = y; // y point minimum
				if( y > ymax ) ymax = y; // y point maximum
			}
		}
	}
	
	// Join bounds
	std::vector< int > bounds;
	bounds.push_back( xmin );
	bounds.push_back( xmax );
	bounds.push_back( ymin );
	bounds.push_back( ymax );
	
	return bounds;
}

// ---------------------------------------------------------------------------
// GET_BRIGHTEST_PIXEL
// Purpose: Gets the brightest pixel in this image.
//
// Parameters:
//		NONE
// ---------------------------------------------------------------------------
std::pair< int, std::pair< int, int > > ImageWrapper::get_brightest_pixel( void ) const {
	
	int xmax = -1;
	int ymax = -1;
	int max = -1;
	
	// Find brightest pixel
	for( int x = 0; x < this->getNRows(); x++ ){
		for( int y = 0; y < this->getNCols(); y++ ){
			
			const int px = this->getPixel( x, y );
			
			if ( px > max ){
				
				max = px; // record new max
				xmax = x; // record x coordinate
				ymax = y; // record y coordinate
			}
		}
	}
	
	// Return ( value, ( x, y ) )
	return std::make_pair( max, std::make_pair( xmax, ymax ) );
}

// ---------------------------------------------------------------------------
// GET_Z
// Purpose: Gets the Z coordinate at a point on the image.
//
// Parameters:
//		Parameter 1: 
// ---------------------------------------------------------------------------
double ImageWrapper::get_z( const double radius, const std::pair< int, int > center, const std::pair< int, int > point ) const {

	// Return z-cz
	return sqrt( ( radius * radius ) - pow( ( point.first - center.first ), 2 ) - pow( ( point.second - center.second ), 2 ) );
}

// ---------------------------------------------------------------------------
// GET_MAGNITUDE
// Purpose: Gets the magnitude.
//
// Parameters:
//		Parameter 1: center coordinates
//		Parameter 2: brightest point coordinates
//		Parameter 3: z-cz
// ---------------------------------------------------------------------------
double ImageWrapper::get_magnitude( const std::pair< int, int > center, std::pair< int, int > point, const double z ) const{
	
	// Return magnitude
	return sqrt( pow( point.first - center.first, 2 ) + pow( point.second - center.second, 2 ) + pow( z, 2 ) );
}

// ---------------------------------------------------------------------------
// DRAW_NORMALS
// Purpose: Draws the normals on the image.
//
// Parameters:
//		Parameter 1: Vector containing 3 XYZ vectors ( 9 elements )
// ---------------------------------------------------------------------------
void ImageWrapper::draw_normals( std::vector< double > vectors
								, const int threshold
								, const int step
								, const ImageWrapper& image2
								, const ImageWrapper& image3 ){
	
	// Compute matrix of minors
	// & Convert to matrix of cofactors
	std::vector< double > cofactors( vectors.size() );
	cofactors[0] = vectors[4] * vectors[8] - vectors[5] * vectors[7]; // +
	cofactors[1] = vectors[3] * vectors[8] - vectors[5] * vectors[6]; // -
	cofactors[1] = -cofactors[1];
	cofactors[2] = vectors[3] * vectors[7] - vectors[4] * vectors[6]; // +
	cofactors[3] = vectors[1] * vectors[8] - vectors[2] * vectors[7]; // -
	cofactors[3] = -cofactors[3];
	cofactors[4] = vectors[0] * vectors[8] - vectors[2] * vectors[6]; // +
	cofactors[5] = vectors[0] * vectors[7] - vectors[1] * vectors[6]; // -
	cofactors[5] = -cofactors[5];
	cofactors[6] = vectors[1] * vectors[5] - vectors[2] * vectors[4]; // +
	cofactors[7] = vectors[0] * vectors[5] - vectors[2] * vectors[3]; // -
	cofactors[7] = -cofactors[7];
	cofactors[8] = vectors[0] * vectors[4] - vectors[1] * vectors[3]; // +
	
	// Calculate determinant by multiplying vectors & cofactors
	double determinant = cofactors[0] * vectors[0] + cofactors[1] * vectors[1] + cofactors[2] * vectors[2];
	
	// Get adjugate/transpose of matrix
	std::swap( cofactors[1], cofactors[3] );
	std::swap( cofactors[2], cofactors[6] );
	std::swap( cofactors[5], cofactors[7] );

	// Multiply 1/determinant by cofactors to get inverse inverse_matrix
	std::vector< double> inverse_matrix;
	for( int i = 0; i < vectors.size(); i++ )
		inverse_matrix.push_back( ( 1.0 / determinant ) * cofactors[i] );
	
	// Check each point
	for( int x = 0; x < this->getNRows(); x += step ){
		for( int y = 0; y < this->getNCols(); y += step ){
			
			const int px1 = this->getPixel ( x, y );
			const int px2 = image2.getPixel( x, y );
			const int px3 = image3.getPixel( x, y );
			
			if( px1 < threshold
			 || px2 < threshold
			 || px3 < threshold ) continue;
			
			// Multiply vectors by vector of pixels
			const double val1 = inverse_matrix[0] * px1 + inverse_matrix[1] * px2 + inverse_matrix[2] * px3;
			const double val2 = inverse_matrix[3] * px1 + inverse_matrix[4] * px2 + inverse_matrix[5] * px3;
			const double val3 = inverse_matrix[6] * px1 + inverse_matrix[7] * px2 + inverse_matrix[8] * px3;
			 
			// Draw line for each point
			Image* temp = this;
			line( temp
				, x
				, y
				, x + 10 * ( val1 / sqrt( pow( val1, 2 ) + pow( val2, 2 ) + pow( val3, 2 ) ) )
				, y + 10 * ( val2 / sqrt( pow( val1, 2 ) + pow( val2, 2 ) + pow( val3, 2 ) ) )
				, 255 );
			
			// Mark starting point with black pixel
			this->setPixel( x, y, 0 );
		}
	}
}

// ---------------------------------------------------------------------------
// DRAW_ALBEDO
// Purpose: Draws the albedo of the image.
//
// Parameters:
//		Parameter 1: Vector containing 3 XYZ vectors ( 9 elements )
//		Parameter 2: threshold
//		Parameter 3: image2
//		Parameter 4: image3
// ---------------------------------------------------------------------------
void ImageWrapper::draw_albedo( std::vector< double > vectors
							, const int threshold
							, const ImageWrapper& image2
							, const ImageWrapper& image3 ){
	
	// Compute matrix of minors
	// & Convert to matrix of cofactors
	std::vector< double > cofactors( vectors.size() );
	cofactors[0] = vectors[4] * vectors[8] - vectors[5] * vectors[7]; // +
	cofactors[1] = vectors[3] * vectors[8] - vectors[5] * vectors[6]; // -
	cofactors[1] = -cofactors[1];
	cofactors[2] = vectors[3] * vectors[7] - vectors[4] * vectors[6]; // +
	cofactors[3] = vectors[1] * vectors[8] - vectors[2] * vectors[7]; // -
	cofactors[3] = -cofactors[3];
	cofactors[4] = vectors[0] * vectors[8] - vectors[2] * vectors[6]; // +
	cofactors[5] = vectors[0] * vectors[7] - vectors[1] * vectors[6]; // -
	cofactors[5] = -cofactors[5];
	cofactors[6] = vectors[1] * vectors[5] - vectors[2] * vectors[4]; // +
	cofactors[7] = vectors[0] * vectors[5] - vectors[2] * vectors[3]; // -
	cofactors[7] = -cofactors[7];
	cofactors[8] = vectors[0] * vectors[4] - vectors[1] * vectors[3]; // +
	
	// Calculate determinant by multiplying vectors & cofactors
	double determinant = cofactors[0] * vectors[0] + cofactors[1] * vectors[1] + cofactors[2] * vectors[2];
	
	// Get adjugate/transpose of matrix
	std::swap( cofactors[1], cofactors[3] );
	std::swap( cofactors[2], cofactors[6] );
	std::swap( cofactors[5], cofactors[7] );

	// Multiply 1/determinant by cofactors to get inverse inverse_matrix
	std::vector< double > inverse_matrix;
	for( int i = 0; i < vectors.size(); i++ )
		inverse_matrix.push_back( ( 1.0 / determinant ) * cofactors[i] );
	
	double max = -1; // init maximum
	
	// First pass through, to get the maximum albedo for later scaling
	for( int x = 0; x < this->getNRows(); x++ ){
		for( int y = 0; y < this->getNCols(); y++ ){
			
			const int px1 = this->getPixel ( x, y );
			const int px2 = image2.getPixel( x, y );
			const int px3 = image3.getPixel( x, y );
			
			if( px1 < threshold
			 || px2 < threshold
			 || px3 < threshold ) continue; // skip
			
			// Multiply vectors by vector of pixels
			const double val1 = inverse_matrix[0] * px1 + inverse_matrix[1] * px2 + inverse_matrix[2] * px3;
			const double val2 = inverse_matrix[3] * px1 + inverse_matrix[4] * px2 + inverse_matrix[5] * px3;
			const double val3 = inverse_matrix[6] * px1 + inverse_matrix[7] * px2 + inverse_matrix[8] * px3;
			
			// Calculate albedo
			const double albedo = sqrt( pow( val1, 2 ) + pow( val2, 2 ) + pow( val3, 2 ) );
			
			// Record max albedo for later scaling
			if( albedo > max ) max = albedo;
		}
	}
	
	// Second pass through, to scale the albedo
	for( int x = 0; x < this->getNRows(); x++ ){
		for( int y = 0; y < this->getNCols(); y++ ){
			
			const int px1 = this->getPixel ( x, y );
			const int px2 = image2.getPixel( x, y );
			const int px3 = image3.getPixel( x, y );
			
			if( px1 < threshold
			 || px2 < threshold
			 || px3 < threshold ) {
				 //this->setPixel( x, y, 0 );
				 continue;
			 }
			
			// Multiply vectors by vector of pixels
			const double val1 = inverse_matrix[0] * px1 + inverse_matrix[1] * px2 + inverse_matrix[2] * px3;
			const double val2 = inverse_matrix[3] * px1 + inverse_matrix[4] * px2 + inverse_matrix[5] * px3;
			const double val3 = inverse_matrix[6] * px1 + inverse_matrix[7] * px2 + inverse_matrix[8] * px3;
			
			// Calculate albedo
			const double albedo = sqrt( pow( val1, 2 ) + pow( val2, 2 ) + pow( val3, 2 ) );
			
			//Scale albedo to range 0-255
			const double scaled_albedo = albedo * 255.0/max;
			
			//Set pixel's scaled albedo
			this->setPixel( x, y, scaled_albedo );	
		}
	}
}

// ---------------------------------------------------------------------------
// SOBEL_FILTER
// Purpose: Runs the sobel filter on this image. 
//			
//
// Parameters:
//		NONE
// ---------------------------------------------------------------------------
void ImageWrapper::sobel_filter( void ){

	// Horizontal sobel kernel
	int horizontal [3][3] = {
		{ -1, 0, 1 },
		{ -2, 0, 2 },
		{ -1, 0, 1 }
	};

	// Vertical sobel kernel
	int vertical [3][3] = {
		{ -1, -2, -1 },
		{ 0, 0, 0 },
		{ 1, 2, 1 }
	};

	const int rows = this->getNRows();
	const int cols = this->getNCols();

	int sum, xsum, ysum;

	int temp [ rows ][ cols ];

	// Get sobel values
	for (int x = 1; x < rows - 1; x++){
		for (int y = 1; y < cols - 1; y++){

			xsum = ysum = 0;
			
			// Perform convolution
			for (int i = -1; i <= 1; i++){
				for (int j = -1; j <= 1; j++){
					
					const int px = this->getPixel( x + i, y + j );
					xsum += px * horizontal[ i + 1 ][ j + 1 ]; // x convolution
					ysum += px * vertical[ i + 1 ][ j + 1 ]; // y convolution
				}
			}
			
			// Compute magnitude
			sum = std::sqrt( xsum * xsum + ysum * ysum );
			
			// Bound checking
			if( sum > 255 ) sum = 255;
			else if( sum < 0 ) sum = 0;

			temp[ x ][ y ] = sum; // Record magnitude
		}
	}
	
	// Write sobel values
	for (int x = 0; x < rows; x++){
		for (int y = 0; y < cols; y++){
			
			this->setPixel( x, y, temp[ x ][ y ] );
		}
	}
}

// ---------------------------------------------------------------------------
// GREYSCALE_TO_BINARY
// Purpose: Converts a greyscale image to a binary image using a threshold 
//			value.
//
// Parameters:
//		Parameter 1: Threshold value
// ---------------------------------------------------------------------------
void ImageWrapper::binary_filter( const double threshold_value ){
	
	for ( int i = 0; i < getNRows(); i++ ){
		for( int k = 0; k < getNCols(); k++ ){
			
			// Get pixel value at i, k
			const int pixel_value = getPixel( i,k );
			
			// Set pixel value at i, k to 0 or 255 (Black or White)
			setPixel( i, k, ( pixel_value < threshold_value ) ? 0 : 255 );
		}
	}
}

// ---------------------------------------------------------------------------
// HOUGH_TRANSFORM
// Purpose: Runs the Hough Transform on this image, producing an output
//			of its Hough Space.
//			
// Parameters:
//		NONE
// ---------------------------------------------------------------------------
void ImageWrapper::hough_transform( void ){

	// Set to 255 for gray-scale variance
	this->setColors( 255 );
	
	const int rows = this->getNRows();
	const int cols = this->getNCols();
	
	const double xcenter = rows/2;
	const double ycenter = cols/2;
	
	// Max radius == hypotenuse (the diagonal) of the entire image
	const double radius_max = hypot( xcenter, ycenter );
	
	// theta = 180 degrees
	const double theta_max = 180;
	
	// Width set to 2 * radius_max to accompany -r values
	const double hough_width = radius_max * 2;
	
	// Height is set to maximum theta
	const double hough_height = theta_max;

	// Initialize accumulator array
	int** acc = new int*[ (int) hough_width ] ();
	for( int i = 0; i < (int) hough_width; i++ )
    	acc[ i ] = new int[ (int) hough_height ] ();

	double max = -1;

	double radius = 0;
	// Create hough space
	for( int x = 0; x < rows; x++ ){
		for( int y = 0; y < cols; y++ ){
			
			const int px = this->getPixel( x, y );
			
			if( px ){
				for (double tr=0.0, t=0; tr<PI; t++, tr+=PI/theta_max){
					
					// Calculate rho using the formula, r = xcos(theta) + ysin(theta)
					radius = ( (double) x - xcenter ) * cos( tr ) + ( (double) y - ycenter ) * sin( tr );
					
					const int rho_index = (int) radius + (int)radius_max;
					const int theta_index = (int) t;
					
					// Increment accumulator value for this pixel
					acc[ rho_index ][ theta_index ]++;
					
					const unsigned int acc_value = acc[ rho_index ][ theta_index ];
					
					// Get the maximum value, needed for later scaling
					if( max == -1 )
						max = acc_value;
					else if( max < acc_value )
						max = acc_value;
				}
			}
		}
	}
	
	// Extend image size to match accumulator
	this->setSize( hough_width, hough_height );
	
	double scaler;
	
	// Create hough space from accumulator 
	for( int x = 0; x < hough_width; x++ ){
		for( int y = 0; y < hough_height; y++ ){
			
			// Scale it to out of 255, since accumulator values can be over 255
			scaler = acc[ x ][ y ] * 255 / max;
			
			this->setPixel( x, y, scaler );
		}
	}
	
	// Clear up memory
	for( int i = 0; i < hough_width; i++ )
		delete[] acc[i];
	
	delete acc;
}

// ---------------------------------------------------------------------------
// HOUGH_LINES
// Purpose: Draws lines on this image based on the Hough Space in parameter 2. 
//			
//
// Parameters:
//		Parameter 1: Threshold for filtering lines
//		Parameter 2: Hough space
// ---------------------------------------------------------------------------
void ImageWrapper::hough_lines( const int threshold, const Image& hspace ){
	
	const int hough_width = hspace.getNRows();
	const int hough_height = hspace.getNCols();
	
	const int rows = this->getNRows();
	const int cols = this->getNCols();
	
	Image* im = this;
	
	// Threshold the hough space
	// Get x and y values for all accumulator bins
	for( int r = 0; r < hough_width; r++ ){
		for ( double tr=0.0, t=0; tr<PI; t++, tr+=PI/hough_height ){
			
			const int bin = hspace.getPixel( r, t );
			
			// Filter out lines below a threshold for clarity
			if( bin >= threshold ){
				
				// Current max
				int max = bin;
			
				// Get local maxima ( 4 left, 4 right )
				for( int i = -4; i <= 4; i++ ){
					for( int j = -4; j <= 4; j++ ){
						
						// Boundary checking
						if( i + r >= 0 && i + r < hough_width
							&& j + t >= 0 && j + t < hough_height){
							
							const int neighbor = hspace.getPixel( i + r, j + t );
								
							// Check if neighbor is bigger
							if( neighbor > max ){
								
								// Skip this pixel since it's not the maximum
								max = neighbor;
								i = j = 5;
							}
						}
					}
				}
				
				// If this pixel is not the max, skip it
				if( bin < max ) continue;
				
				// Skip corner case where r == hough_width/2
				if( r == hough_width/2 ) continue;
				
				double x, y, x2, y2;
				x = y = x2 = y2 = 0;
				
				// Get x and y coordinates using the formula:
				// r = xcos(theta) + ysin(theta)
				if( ( 45 <= t && t <= 135 ) ){
					x = 0; // start with x at 0
					y = ( ( (double)r - hough_width/2 ) - ( x - this->getNRows()/2 ) * cos( tr ) ) / sin( tr ) + this->getNCols()/2;
					
					x2 = this->getNRows() - 1; // set y to max height - 1
					y2 = ( ( (double)r - hough_width/2 ) - ( x2 - this->getNRows()/2 ) * cos( tr ) ) / sin( tr ) + this->getNCols()/2;
				}
				else{
					y = 0; // start with y at 0
					x = ( ( (double)r - hough_width/2 ) - ( y - this->getNCols()/2 ) * sin( tr ) ) / cos( tr ) + this->getNRows()/2; // (x, 0)
					
					y2 = this->getNCols() - 1; // set y to max width - 1
					x2 = ( ( (double)r - hough_width/2 ) - ( y2 - this->getNCols()/2 ) * sin( tr ) ) / cos( tr ) + this->getNRows()/2; // (x, height - 1)
				}

				// Correct any bounding issues
				if( x < 0 ) x = 0;
				if( x > this->getNRows() - 1 ) x = this->getNRows() - 1;
				if( x2 < 0 ) x2 = 0;
				if( x2 > this->getNRows() - 1 ) x2 = this->getNRows() - 1;
				
				// Draw the line
				line( im, x, y, x2, y2, 255 );
			}
		}
	}
}