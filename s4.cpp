// ---------------------------------------------------------------------------
// s4.cpp
// Creates a albedo based on the normal vectors to the brightest spots
// in 3 image objects.
// 
// Author: Andrew Miloslavsky
// Date: November 15th, 2015
// ---------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include "ImageWrapper.h"

int main(int argc, char** argv){

	if( argc < 7 ) return -1;
	
	const char* INPUT_FILE = argv[ 1 ];
	const char* IMAGE1 = argv[ 2 ];
	const char* IMAGE2 = argv[ 3 ];
	const char* IMAGE3 = argv[ 4 ];
	const int THRESHOLD = atoi( argv[ 5 ] );
	const char* OUTPUT_IMAGE = argv[ 6 ];
	
	// Load images
	ImageWrapper* im1 = new ImageWrapper( IMAGE1 );
	ImageWrapper* im2 = new ImageWrapper( IMAGE2 );
	ImageWrapper* im3 = new ImageWrapper( IMAGE3 );
	
	// Load input file
	std::ifstream is( INPUT_FILE );
	
	// Parse the line in text file
	std::vector< double > vals; // store x,y,z
	std::string line;
	while( std::getline( is, line ) ){
		
		std::istringstream iss ( line );
		
		std::string x_s;
		std::string y_s;
		std::string z_s;
		
		iss >> x_s; // read x
		iss >> y_s; // read y
		iss >> z_s; // read r
		
		// Convert x,y,z to doubles
		vals.push_back( atof( x_s.c_str() ) );
		vals.push_back( atof( y_s.c_str() ) );
		vals.push_back( atof( z_s.c_str() ) );
	}
	
	im1->draw_albedo( vals, THRESHOLD, *im2, *im3 ); // draw albedo
	
	im1->save( OUTPUT_IMAGE ); // save albedo
	
	// Deallocate
	delete im1;
	delete im2;
	delete im3;

	return 0;
}