// ---------------------------------------------------------------------------
// s3.cpp
// Creates a "needle-map" based on the normal vectors to the brightest spots
// in 3 objects.
// 
// Author: Andrew Miloslavsky
// Date: November 15th, 2015
// ---------------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <sstream>
#include "ImageWrapper.h"

int main( int argc, char** argv ){
	
	if( argc < 8 ) return -1;
	
	const char* INPUT_FILE = argv[ 1 ];
	const char* IMAGE1 = argv[ 2 ];
	const char* IMAGE2 = argv[ 3 ];
	const char* IMAGE3 = argv[ 4 ];
	const int STEP = atoi( argv[ 5 ] );
	const int THRESHOLD = atoi( argv[ 6 ] );
	const char* OUTPUT_IMAGE = argv[ 7 ];
	
	// Load images
	ImageWrapper* im1 = new ImageWrapper( IMAGE1 );
	ImageWrapper* im2 = new ImageWrapper( IMAGE2 );
	ImageWrapper* im3 = new ImageWrapper( IMAGE3 );
	
	std::ifstream is( INPUT_FILE );
	
	// Parse the line in text file
	std::vector< double > vals; // Store x,y,z as doubles
	std::string line;
	while( std::getline( is, line ) ){
		
		std::istringstream iss ( line );
		
		std::string x_s;
		std::string y_s;
		std::string z_s;
		
		iss >> x_s; // read x
		iss >> y_s; // read y
		iss >> z_s; // read r
		
		// Convert x,y,z to doubles
		vals.push_back( atof( x_s.c_str() ) );
		vals.push_back( atof( y_s.c_str() ) );
		vals.push_back( atof( z_s.c_str() ) );
	}
	
	// Draw normals
	im1->draw_normals( vals, THRESHOLD, STEP, *im2, *im3 );
	
	im1->save( OUTPUT_IMAGE ); // save output image

	// Deallocate memory
	delete im1;
	delete im2;
	delete im3;
	
	return 0;
}